package com.reinaldo.ws.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.reinaldo.ws.rest.vo.Cadena;


@Path("/ServiceReinaldo")
public class ServiceLogin {
	
	@GET
	@Path("/candidate")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public String candidate() {
		String nombre = "Reinaldo Vargas";
        return nombre;
	}
	
	@POST
	@Path("/compress")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Cadena compress(Cadena cadena) {
		int countConsecutive = 0;
        for (int i = 0; i < cadena.getCadena().length(); i++) {
          countConsecutive++;
          if (i + 1 >= cadena.getCadena().length() || cadena.getCadena().toUpperCase().charAt(i) != cadena.getCadena().toUpperCase().charAt(i + 1)) {
              cadena.setCadena(cadena.getCadena().toUpperCase().replaceFirst(cadena.getCadena().substring(i+1-countConsecutive, i+1), countConsecutive+""+cadena.getCadena().toUpperCase().charAt(i)));
              i=i-countConsecutive+1+(countConsecutive+"").length();
              countConsecutive = 0;
          }
        }
        return cadena;
	}
	
}
